package com.example.aleks.testinglab4;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by aleks on 26.12.2016.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class MainAcivityTest {
    private MainActivity activity;
    private TransferData data;

    {
        activity=Robolectric.setupActivity(MainActivity.class);
        data=new TransferDataStub(activity);
        activity.setTransferData(data);
    }

    @Test
    public void shouldNotBeNull() {
        assertNotNull(activity);
    }
    @Test
    public void getDataTest(){
        List<Student> list=data.getStudentsList();
        Assert.assertNotNull(list);
    }
    @Test
    public void postDataTest(){
        int size=data.getStudentsList().size();
        List<Student> list=data.postStudent("test","test","test","1.5");
        Assert.assertEquals(size+1,list.size());
    }
    @Test
    public void deleteDataTest(){
        List<Student> list=data.postStudent("test","test","test","1.5");
        int size=data.getStudentsList().size();
        int id=data.getStudentsList().get(list.size()-1).getId();
        data.delStudent(id);
        List<Student> students=data.getStudentsList();
        Assert.assertEquals(size-1,students.size());
    }

    @Test
    public void updateDataTest(){
        List<Student> students=data.getStudentsList();
        int id=students.get(students.size()-1).getId();
        students=data.updateStudent(id,"name","name");
        String name=students.get(students.size()-1).getName();
        Assert.assertEquals("name",name);
    }

    @Test
    public void usingMockJson(){
        JSONParser parser=mock(JSONParser.class);
        List<Student> list=data.getStudentsList();
        String DATA="[\n" +
                "  {\n" +
                "    \"id\": 1,\n" +
                "    \"surname\": \"Smovhzenko\",\n" +
                "    \"name\": \"Oleksiys\",\n" +
                "    \"patronymic\": \"leonidovych\",\n" +
                "    \"general_point\": 5.099999904632568\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\": 3,\n" +
                "    \"surname\": \"Krasnuk\",\n" +
                "    \"name\": \"Igosssr\",\n" +
                "    \"patronymic\": \"Oleksandrovych\",\n" +
                "    \"general_point\": 4\n" +
                "  }\n" +
                "]";
        when(parser.getFromJson(anyString())).thenReturn(list);
        when(parser.putTOJson(anyListOf(Student.class))).thenReturn(DATA);
        data.setJsonParser(parser);
        List<Student> studentsList=data.getStudentsList();
        Assert.assertEquals(studentsList.size(),2);
    }
}
