package com.example.aleks.testinglab4;

/**
 * Created by aleks on 25.12.2016.
 */

public class Student {
    private int id;
    private String surname;
    private String name;
    private String patronymic;
    private double general_point;

    public Student() {
    }

    public Student(int id, String surname, String name, String patronymic, double general_point) {
        this.id = id;
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.general_point = general_point;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public double getGeneral_point() {
        return general_point;
    }

    public void setGeneral_point(double general_point) {
        this.general_point = general_point;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", surname='" + surname + '\'' +
                ", name='" + name + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", general_point=" + general_point +
                '}';
    }
}
