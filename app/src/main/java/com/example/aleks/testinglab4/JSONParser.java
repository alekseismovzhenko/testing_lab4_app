package com.example.aleks.testinglab4;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

/**
 * Created by aleks on 26.12.2016.
 */

public class JSONParser {

    private Gson gson;

    public JSONParser() {
        this.gson=new Gson();
    }

    public List<Student> getFromJson(String string){
        return gson.fromJson(string,new TypeToken<List<Student>>(){}.getType());
    }

    public String putTOJson(List<Student> students){
        return gson.toJson(students);
    }
}
