package com.example.aleks.testinglab4;

import java.util.List;

/**
 * Created by aleks on 25.12.2016.
 */

public interface activityCallBack {
    void updateListView(List<Student> students);
    void update();
}
