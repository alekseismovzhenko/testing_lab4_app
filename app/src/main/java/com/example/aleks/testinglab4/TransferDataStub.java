package com.example.aleks.testinglab4;

import android.content.Context;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by aleks on 26.12.2016.
 */

public class TransferDataStub extends TransferData {

    private List<Student> studentList;

    private static final String FILENAME="data.txt";
    private static final String DATA="[\n" +
            "  {\n" +
            "    \"id\": 1,\n" +
            "    \"surname\": \"Smovhzenko\",\n" +
            "    \"name\": \"Oleksiys\",\n" +
            "    \"patronymic\": \"leonidovych\",\n" +
            "    \"general_point\": 5.099999904632568\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\": 3,\n" +
            "    \"surname\": \"Krasnuk\",\n" +
            "    \"name\": \"Igosssr\",\n" +
            "    \"patronymic\": \"Oleksandrovych\",\n" +
            "    \"general_point\": 4\n" +
            "  }\n" +
            "]";

    public TransferDataStub(MainActivity activity) {
        super(activity);
        File file = new File(getActivity().getFilesDir(), FILENAME);
        if(!file.exists())
            writeToFile(DATA);
    }

    @Override
    public List<Student> getStudentsList() {
        String response=readFromFile();
        studentList=getJsonParser().getFromJson(response);
        getActivity().updateListView(studentList);
        return studentList;
    }

    @Override
    public List<Student> postStudent(String name, String surname, String patronymic, String point) {
        getStudentsList();
        Student student=studentList.get(studentList.size()-1);
        studentList.add(new Student(student.getId()+1,surname,name,patronymic,Double.parseDouble(point)));
        writeToFile(getJsonParser().putTOJson(studentList));
        getActivity().updateListView(studentList);
        return studentList;
    }

    @Override
    public void delStudent(int id) {
        for (Student student:studentList) {
            if(student.getId()==id) {
                studentList.remove(student);
                break;
            }
        }
        writeToFile(getJsonParser().putTOJson(studentList));
        getActivity().updateListView(studentList);
    }

    @Override
    public List<Student> deleteStudent(int id) {
        return new ArrayList<>();
    }

    @Override
    public List<Student> updateStudent(int id, String column, String data) {
        for (Student student:studentList) {
            if(student.getId()==id) {
                switch (column){
                    case "surname":{
                        student.setSurname(data);
                        break;
                    }
                    case "name":{
                        student.setName(data);
                        break;
                    }
                    case "patronymic":{
                        student.setPatronymic(data);
                        break;
                    }
                    case "general_point":{
                        student.setGeneral_point(Double.parseDouble(data));
                        break;
                    }
                }
                break;
            }
        }
        writeToFile(getJsonParser().putTOJson(studentList));
        return studentList;
    }

    private void writeToFile(String string){
        try {
            OutputStreamWriter writer=new OutputStreamWriter(getActivity().openFileOutput(FILENAME,Context.MODE_PRIVATE));
            writer.write(string);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
//        File file = new File(getActivity().getFilesDir(), FILENAME);
//        if(!file.exists()){
//            try {
//                file.createNewFile();
//                BufferedWriter writer=new BufferedWriter(new FileWriter(file,false));
//                writer.write(string);
//                writer.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }else {
//            try {
//                BufferedWriter writer=new BufferedWriter(new FileWriter(file,false));
//                writer.write(string);
//                writer.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
   }

    private String readFromFile(){
        try {
            InputStream inputStream=getActivity().openFileInput(FILENAME);
            if(inputStream!=null){
                Scanner scanner=new Scanner(inputStream);
                StringBuilder builder=new StringBuilder();
                while (scanner.hasNextLine())
                    builder.append(scanner.nextLine());
                return builder.toString();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return "";
    }


    public List<Student> getStudentList() {
        return studentList;
    }
}
