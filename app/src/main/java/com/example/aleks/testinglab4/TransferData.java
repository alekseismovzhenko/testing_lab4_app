package com.example.aleks.testinglab4;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.Formatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by aleks on 25.12.2016.
 */

public class TransferData {

    private final static String URL_GET="http://192.168.56.1:8060/server/students/getall";
    private final static String URL_POST="http://192.168.56.1:8060/server/students/add";
    private final static String URL_DELETE="http://192.168.56.1:8060/server/students/delete";
    private final static String URL_PUT="http://192.168.56.1:8060/server/students/update";

    private JSONParser jsonParser;

    private MainActivity activity;

    public TransferData(MainActivity activity) {
        this.activity = activity;
        jsonParser=new JSONParser();
        Log.d("mytag","initialize activity");
    }


    public List<Student> getStudentsList(){
            List<Student> studentList=new ArrayList<>();
            RequestQueue queue = Volley.newRequestQueue(activity);
            final List<Student> studentsList=new ArrayList<>();
            StringRequest request=new StringRequest(Request.Method.GET, URL_GET, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    JSONParser parser=new JSONParser();
                    List<Student> students=parser.getFromJson(response);
                    Log.d("mytag",String.valueOf(students.size()));
                    activity.updateListView(students);
                }
            },new Response.ErrorListener(){

                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }
            );
        queue.add(request);
        Log.d("mytag",String.valueOf(studentsList.size()));
        return studentList;
    }
    public List<Student> postStudent(final String name, final String surname, final String patronymic, final String point){
        List<Student> studentList=new ArrayList<>();
        RequestQueue queue=Volley.newRequestQueue(activity);
        //String string="?name=test&surname=test&patronymic=test&general_point=3.5";
        String string=String.format("?name=%s&surname=%s&patronymic=%s&general_point=%s",name,surname,patronymic,point);
        StringRequest request=new StringRequest(Request.Method.POST, URL_POST+string,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        activity.update();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }){

        };
        Log.d("mytag",request.toString());
        queue.add(request);
        return studentList;
    }
    public void delStudent(int id){
        String string="?id="+id;
        RequestQueue queue=Volley.newRequestQueue(activity);
        //Log.d("mytag",URL_DELETE+string);
        StringRequest request=new StringRequest(Request.Method.DELETE, URL_DELETE+string,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        queue.add(request);
        activity.update();
    }
    public List<Student> deleteStudent(int id){
        List<Student> studentList=new ArrayList<>();
        RequestQueue queue = Volley.newRequestQueue(activity);
        final List<Student> studentsList=new ArrayList<>();
        StringRequest request=new StringRequest(Request.Method.GET, URL_GET, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Gson gson=new Gson();
                List<Student> students=gson.fromJson(response,new TypeToken<List<Student>>(){}.getType());
            }
        },new Response.ErrorListener(){

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }
        );
        queue.add(request);
        Log.d("mytag",String.valueOf(studentsList.size()));
        return studentList;
    }

    public List<Student> updateStudent(int id,String column,String data){
        List<Student> studentList=new ArrayList<>();
        String string=String.format("?id=%s&column=%s&data=%s",id,column,data);
        Log.d("mytag",URL_PUT+string);
        RequestQueue queue=Volley.newRequestQueue(activity);
        StringRequest request=new StringRequest(Request.Method.PUT, URL_PUT+string,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        queue.add(request);
        return studentList;
    }

    public MainActivity getActivity() {
        return activity;
    }

    public void setActivity(MainActivity activity) {
        this.activity = activity;
    }

    public JSONParser getJsonParser() {
        return jsonParser;
    }

    public void setJsonParser(JSONParser jsonParser) {
        this.jsonParser = jsonParser;
    }
}
