package com.example.aleks.testinglab4;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements activityCallBack,Serializable{

    private final static String URL="http://192.168.56.1:8060/server/students/getall";

    private ListView listView;

    public List<Student> getList() {
        return list;
    }

    public void setList(List<Student> list) {
        this.list = list;
    }

    private List<Student> list;
    private TransferData transferData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.listView);

        list = new ArrayList<>();

        listView.addHeaderView(getLayoutInflater().inflate(R.layout.list_header,null));
        //listView.setAdapter(new MyArrayAdapter(this,list));
        setTransferData(new TransferDataStub(this));
        transferData.getStudentsList();
        listView.setAdapter(new MyArrayAdapter(this,list));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.newItemMenu:{
                newItem();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void newItem(){
        DialogFragment dialogFragment=AddDialogFragment.newInstance(this);
        dialogFragment.show(getSupportFragmentManager(),"dialog");
    }

    @Override
    public void updateListView(List<Student> students) {
        list=students;
        listView.setAdapter(new MyArrayAdapter(this,list));
    }

    @Override
    public void update() {
        transferData.getStudentsList();
    }

    public TransferData getTransferData() {
        return transferData;
    }

    public void setTransferData(TransferData transferData) {
        this.transferData = transferData;
    }


    private class MyArrayAdapter extends ArrayAdapter<Student>{

        public MyArrayAdapter(Context context, List<Student> objects) {
            super(context, R.layout.item, objects);
        }
        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Student student = getItem(position);
            final int id=student.getId();
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext())
                        .inflate(R.layout.item, null);
            }
            ((TextView) convertView.findViewById(R.id.idTextView)).setText(String.valueOf(student.getId()));
            EditText surnameEditText=(EditText) convertView.findViewById(R.id.surnameEditText);
            surnameEditText.setText(student.getSurname());
            surnameEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                private String text;
                @Override
                public void onFocusChange(View view, boolean b) {
                    if(b) {
                        text=((EditText)view).getText().toString();
                    }
                    else {
                        if(!((EditText)view).getText().toString().equals(text))
                            transferData.updateStudent(id,"surname",((EditText)view).getText().toString());
                    }
                }
            });
            EditText nameEditText=(EditText) convertView.findViewById(R.id.nameEditText);
            nameEditText.setText(student.getName());
            nameEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                private String text;
                @Override
                public void onFocusChange(View view, boolean b) {
                    if(b) {
                        text=((EditText)view).getText().toString();
                    }
                    else {
                        if(!((EditText)view).getText().toString().equals(text))
                            transferData.updateStudent(id,"name",((EditText)view).getText().toString());
                    }
                }
            });
            EditText patronymicEditText=(EditText) convertView.findViewById(R.id.patronymicEditText);
            patronymicEditText.setText(student.getPatronymic());
            patronymicEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                private String text;
                @Override
                public void onFocusChange(View view, boolean b) {
                    if(b) {
                        text=((EditText)view).getText().toString();
                    }
                    else {
                        if(!((EditText)view).getText().toString().equals(text))
                            transferData.updateStudent(id,"patronymic",((EditText)view).getText().toString());
                    }
                }
            });
            EditText pointEditText=(EditText) convertView.findViewById(R.id.generalPointEditText);
            pointEditText.setText(String.valueOf(student.getGeneral_point()));
            pointEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                private String text;
                @Override
                public void onFocusChange(View view, boolean b) {
                    if(b) {
                        text=((EditText)view).getText().toString();
                    }
                    else {
                        if(!((EditText)view).getText().toString().equals(text))
                            transferData.updateStudent(id,"general_point",((EditText)view).getText().toString());
                    }
                }
            });
            convertView.findViewById(R.id.deleteButton).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    transferData.delStudent(id);
                    transferData.deleteStudent(id);
                }
            });
            return convertView;
        }
    }
}
