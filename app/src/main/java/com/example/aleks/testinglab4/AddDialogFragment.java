package com.example.aleks.testinglab4;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AddDialogFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddDialogFragment extends DialogFragment{

    private static final String DESCRIBABLE_KEY = "describable_key";

    private MainActivity activity;

    public AddDialogFragment() {
        // Required empty public constructor
    }


    public static AddDialogFragment newInstance(MainActivity activity) {
        AddDialogFragment fragment = new AddDialogFragment();
        Bundle bundle=new Bundle();
        fragment.setActivity(activity);
        bundle.putSerializable(DESCRIBABLE_KEY,activity);
        fragment.setArguments(bundle);
        return fragment;
    }
/*
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder=new AlertDialog.Builder(getContext());
        //View view=getLayoutInflater(savedInstanceState).inflate(R.layout.fragment_add_dialog,null);
        builder.setView(R.layout.fragment_add_dialog);
        builder.setTitle("Add new record").setPositiveButton("Add",this).setNegativeButton("Cancel",this);
        return builder.create();
    }
*/
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_add_dialog,null);
        activity=(MainActivity)getArguments().getSerializable(DESCRIBABLE_KEY);
        final EditText nameEditText,surnameEditText,patronymicEditText,pointEditText;
        nameEditText=(EditText)view.findViewById(R.id.addNameEditText);
        surnameEditText=(EditText)view.findViewById(R.id.addSurnameEditText);
        patronymicEditText=(EditText)view.findViewById(R.id.addPatronymicEditText);
        pointEditText=(EditText)view.findViewById(R.id.addGeneralPointEditText);
        getDialog().setTitle("Add new student");
        view.findViewById(R.id.addButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name=nameEditText.getText().toString();
                String surname=surnameEditText.getText().toString();
                String patronymic=patronymicEditText.getText().toString();
                String point=pointEditText.getText().toString();
                if(!(name.equals("")||surname.equals("")||patronymic.equals("")||point.equals(""))) {
                    activity.getTransferData().postStudent(name,
                            surname,
                            patronymic,
                            point);
                    dismiss();
                }
            }
        });
        view.findViewById(R.id.cancelButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        return view;
    }

    public void setActivity(MainActivity activity) {
        this.activity = activity;
    }
/*
    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        switch (i){
            case Dialog.BUTTON_POSITIVE:{
                Toast.makeText(getContext(),"added",Toast.LENGTH_SHORT).show();
                break;
            }
            case Dialog.BUTTON_NEGATIVE:{
                Toast.makeText(getContext(),"cancelled",Toast.LENGTH_SHORT).show();
                break;
            }
        }
    }
    */
}
